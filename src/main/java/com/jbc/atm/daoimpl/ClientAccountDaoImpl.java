package com.jbc.atm.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jbc.atm.architecture.Exception.BusinessException;
import com.jbc.atm.dao.ClientAccountDao;
import com.jbc.atm.dto.ClientAccountBalanceRequest;
import com.jbc.atm.dto.ClientAccountBalanceResponse;
import com.jbc.atm.mapper.AtmRowMapper;
import com.jbc.atm.mapper.ClientAccountRowMapper;
import com.jbc.atm.model.Atm;

@Repository
public class ClientAccountDaoImpl implements ClientAccountDao{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public Integer validateAccountNumberAndPin(Integer accountNumber, Integer pin) throws BusinessException {
		Object[] params = {accountNumber, pin};
		
		Integer retornedObj = null;
		try {
			retornedObj = jdbcTemplate.queryForObject("SELECT * FROM public.validateAccountNumberAndPin (?,?);",params,Integer.class);
		} catch (Exception e) {
			retornedObj = -1000;
		}
		return retornedObj;
	}
	
	@Override
	public ClientAccountBalanceResponse findBalanceByAccountNumberAndPin(Integer accountNumber, Integer pin) throws BusinessException {
		Object[] params = {accountNumber, pin};
		
		ClientAccountBalanceResponse retornedObj = null;
		try {
			retornedObj = jdbcTemplate.queryForObject("SELECT * FROM public.findBalanceByAccountNumberAndPin (?,?);",
					params,
					new ClientAccountRowMapper());
		} catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}
		return retornedObj;
	}
	
	@Override
	public ClientAccountBalanceResponse updateBalance(Integer accountNumber, Integer pin,Integer amount) {
		Object[] params = {accountNumber, pin, amount};
		
		ClientAccountBalanceResponse retornedObj = null;
		try {
			retornedObj = jdbcTemplate.queryForObject("SELECT * FROM public.updateClientAccount (?,?,?);",
					params,
					new ClientAccountRowMapper());
		} catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}
		return retornedObj;
	}

}
