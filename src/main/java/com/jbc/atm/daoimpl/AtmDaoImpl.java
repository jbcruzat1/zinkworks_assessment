package com.jbc.atm.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jbc.atm.dao.AtmDao;
import com.jbc.atm.mapper.AtmRowMapper;
import com.jbc.atm.model.Atm;

@Repository
public class AtmDaoImpl implements AtmDao{
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public Atm getAtmBalance() {
		Object[] params = {};
		
		Atm retornedObj = null;
		try {
			retornedObj = jdbcTemplate.queryForObject("SELECT * FROM public.findNotesAtm ();",
					params,
					new AtmRowMapper());
		} catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}
		return retornedObj;
	}

	@Override
	public Atm updateBalance(int[] noteCounter) {
		Object[] params = {noteCounter[0],noteCounter[1],noteCounter[2],noteCounter[3]};
		
		Atm retornedObj = null;
		try {
			retornedObj = jdbcTemplate.queryForObject("SELECT * FROM public.updateBalance (?,?,?,?);",
					params,
					new AtmRowMapper());
		} catch (EmptyResultDataAccessException e) {
			retornedObj = null;
		}
		return retornedObj;
	}
	
	

}
