package com.jbc.atm.dao;

import com.jbc.atm.architecture.Exception.BusinessException;
import com.jbc.atm.dto.ClientAccountBalanceRequest;
import com.jbc.atm.dto.ClientAccountBalanceResponse;

public interface ClientAccountDao {
	
	public Integer validateAccountNumberAndPin(Integer accountNumber, Integer pin) throws BusinessException;
	public ClientAccountBalanceResponse findBalanceByAccountNumberAndPin(Integer accountNumber, Integer pin) throws BusinessException;
	public ClientAccountBalanceResponse updateBalance(Integer accountNumber, Integer pin,Integer amount) throws BusinessException;
}
