package com.jbc.atm.dao;

import com.jbc.atm.model.Atm;

public interface AtmDao {
	
	public Atm getAtmBalance();
	public Atm updateBalance(int[] noteCounter);

}
