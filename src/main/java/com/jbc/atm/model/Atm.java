package com.jbc.atm.model;

public class Atm {
	public Double total;
	public Integer fiftys;
	public Integer twentys;
	public Integer tens;
	public Integer fives;
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Integer getFiftys() {
		return fiftys;
	}
	public void setFiftys(Integer fiftys) {
		this.fiftys = fiftys;
	}
	public Integer getTwentys() {
		return twentys;
	}
	public void setTwentys(Integer twentys) {
		this.twentys = twentys;
	}
	public Integer getTens() {
		return tens;
	}
	public void setTens(Integer tens) {
		this.tens = tens;
	}
	public Integer getFives() {
		return fives;
	}
	public void setFives(Integer fives) {
		this.fives = fives;
	}
	
	
}
