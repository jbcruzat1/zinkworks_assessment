package com.jbc.atm.architecture.Model;

public class Message<T> {
	private String message;
	private Integer code;
	private T object;

	public Message(String message, Integer code, T T) {
		this.message = message;
		this.code = code;
		this.object = T;
	}

	public Message(T T) {
		this.message = "Success.";
		this.code = 200;
		this.object =T;
	}
	
	public Message() {
		this.message = "Success.";
		this.code = 200;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

}
