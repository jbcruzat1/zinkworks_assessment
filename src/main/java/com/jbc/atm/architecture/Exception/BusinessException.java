package com.jbc.atm.architecture.Exception;

public class BusinessException extends Exception {
	private static final long serialVersionUID = 1516940146496765816L;

	public BusinessException(String message) {
		super(message);
	}

}