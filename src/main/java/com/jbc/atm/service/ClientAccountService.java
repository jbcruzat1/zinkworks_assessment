package com.jbc.atm.service;

import com.jbc.atm.architecture.Exception.BusinessException;
import com.jbc.atm.dto.ClientAccountBalanceRequest;
import com.jbc.atm.dto.ClientAccountBalanceResponse;
import com.jbc.atm.dto.WithDrawRequest;
import com.jbc.atm.dto.WithDrawResponse;

public interface ClientAccountService {
	
	public ClientAccountBalanceResponse getBalance(ClientAccountBalanceRequest request) throws BusinessException;
	public WithDrawResponse withdraw(WithDrawRequest request) throws BusinessException;
}
