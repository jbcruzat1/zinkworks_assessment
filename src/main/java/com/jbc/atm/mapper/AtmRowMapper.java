package com.jbc.atm.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.jbc.atm.model.Atm;

public class AtmRowMapper implements RowMapper<Atm>{

	@Override
	public Atm mapRow(ResultSet rs, int rowNum) throws SQLException {
		Atm atm = new Atm();
		atm.setTotal(rs.getDouble("total"));
		atm.setFiftys(rs.getInt("fiftys"));
		atm.setTwentys(rs.getInt("twentys"));
		atm.setTens(rs.getInt("tens"));
		atm.setFives(rs.getInt("fives"));
		return atm;
	}
	
}
