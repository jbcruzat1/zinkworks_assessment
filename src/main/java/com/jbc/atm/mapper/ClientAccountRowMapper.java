package com.jbc.atm.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.jbc.atm.dto.ClientAccountBalanceResponse;


public class ClientAccountRowMapper implements RowMapper<ClientAccountBalanceResponse> {

	@Override
	public ClientAccountBalanceResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ClientAccountBalanceResponse obj = new ClientAccountBalanceResponse();
		
		obj.setAccountNumber(rs.getInt("accountNumber"));
		obj.setOpeningBalance(rs.getInt("openingBalance"));
		obj.setOverdraft(rs.getInt("overdraft"));
		
		return obj;
	}

}

