package com.jbc.atm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jbc.atm.architecture.Exception.BusinessException;
import com.jbc.atm.architecture.Model.Message;
import com.jbc.atm.dto.ClientAccountBalanceRequest;
import com.jbc.atm.dto.ClientAccountBalanceResponse;
import com.jbc.atm.dto.WithDrawRequest;
import com.jbc.atm.dto.WithDrawResponse;
import com.jbc.atm.service.ClientAccountService;

@RestController
public class ClientAccountRestController {
	@Autowired
	ClientAccountService atmService;
	
	@GetMapping(path = "/client/balance")
	public Message<ClientAccountBalanceResponse> balance(@RequestBody ClientAccountBalanceRequest request) throws BusinessException{
		Message<ClientAccountBalanceResponse> response = new Message<ClientAccountBalanceResponse>();
		try {
			response.setObject(atmService.getBalance(request));
		} catch (BusinessException e) {
			response.setCode(422);
			response.setMessage(e.getMessage());
			response.setObject(null);
		}catch (Exception e) {
			response.setCode(500);
			response.setMessage(e.getMessage());
			response.setObject(null);
		}
		return response;
	}
	
	@PostMapping(path = "/client/withdrawal")
	public Message<WithDrawResponse> withdraw(@RequestBody WithDrawRequest request) throws BusinessException{
		Message<WithDrawResponse> response = new Message<WithDrawResponse>();
		try {
			response.setObject(atmService.withdraw(request));
		} catch (BusinessException e) {
			response.setCode(422);
			response.setMessage(e.getMessage());
			response.setObject(null);
		}catch (Exception e) {
			response.setCode(500);
			response.setMessage(e.getMessage());
			response.setObject(null);
		}
		return response;
	}
	
}
