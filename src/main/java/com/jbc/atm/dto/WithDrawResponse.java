package com.jbc.atm.dto;

import com.jbc.atm.model.Atm;

public class WithDrawResponse {
	
	Atm atm;
	public Integer accountNumber;
	public Integer openingBalance;
	public Integer overdraft;
	public String quantityNotes;
	public Integer maxWithDrawal;
	
	public Atm getAtm() {
		return atm;
	}
	public void setAtm(Atm atm) {
		this.atm = atm;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Integer openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Integer getOverdraft() {
		return overdraft;
	}
	public void setOverdraft(Integer overdraft) {
		this.overdraft = overdraft;
	}
	public String getQuantityNotes() {
		return quantityNotes;
	}
	public void setQuantityNotes(String quantityNotes) {
		this.quantityNotes = quantityNotes;
	}
	public Integer getMaxWithDrawal() {
		return maxWithDrawal;
	}
	public void setMaxWithDrawal(Integer maxWithDrawal) {
		this.maxWithDrawal = maxWithDrawal;
	}
	

}
