package com.jbc.atm.dto;

public class ClientAccountBalanceResponse {
	public Integer accountNumber;
	public Integer openingBalance;
	public Integer overdraft;
	public Integer maxWithDrawal;
	
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Integer openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Integer getOverdraft() {
		return overdraft;
	}
	public void setOverdraft(Integer overdraft) {
		this.overdraft = overdraft;
	}
	public Integer getMaxWithDrawal() {
		return maxWithDrawal;
	}
	public void setMaxWithDrawal(Integer maxWithDrawal) {
		this.maxWithDrawal = maxWithDrawal;
	}

	
}
