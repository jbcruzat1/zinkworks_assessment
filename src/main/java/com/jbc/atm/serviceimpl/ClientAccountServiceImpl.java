package com.jbc.atm.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jbc.atm.architecture.Exception.BusinessException;
import com.jbc.atm.dao.AtmDao;
import com.jbc.atm.dao.ClientAccountDao;
import com.jbc.atm.dto.ClientAccountBalanceRequest;
import com.jbc.atm.dto.ClientAccountBalanceResponse;
import com.jbc.atm.dto.WithDrawRequest;
import com.jbc.atm.dto.WithDrawResponse;
import com.jbc.atm.model.Atm;
import com.jbc.atm.service.ClientAccountService;

@Service
public class ClientAccountServiceImpl implements ClientAccountService{
	@Autowired
	ClientAccountDao clientAccountDao;
	@Autowired
	AtmDao atmDao;
	
	
	@Override
	public ClientAccountBalanceResponse getBalance(ClientAccountBalanceRequest request) throws BusinessException {
		ClientAccountBalanceResponse clientAccount = new ClientAccountBalanceResponse();
		//validations
		//account number
		if(request.getAccountNumber() == null || request.getAccountNumber().equals("")) {
			throw new BusinessException("Account Number cannot be null.");
		}
		
		//PIN
		if(request.getPin() == null || request.getPin().equals("")) {
			throw new BusinessException("PIN cannot be null.");
		}
		
		//Validating username and password in BD
		Integer validation = clientAccountDao.validateAccountNumberAndPin(request.getAccountNumber(), request.getPin());
		if(validation == -1) {
			throw new BusinessException("AccountNumber does not exists.");
		}else if(validation == -2) {
			throw new BusinessException("PIN Incorrect.");
		}else if (validation == 0) {
			clientAccount = clientAccountDao.findBalanceByAccountNumberAndPin(request.getAccountNumber(), request.getPin());
			clientAccount.setMaxWithDrawal(clientAccount.getOpeningBalance() + clientAccount.getOverdraft());
		}
		
		//returning client account
		return clientAccount;
	}
	
	
	@Override
	public WithDrawResponse withdraw(WithDrawRequest request) throws BusinessException {
		ClientAccountBalanceResponse clientAccount = new ClientAccountBalanceResponse();
		Atm atm = new Atm();
		WithDrawResponse withDrawResponse = new WithDrawResponse();
		
		//validations
		//account number -	should not dispense funds if account number is incorrect,
		if(request.getAccountNumber() == null || request.getAccountNumber().equals("")) {
			throw new BusinessException("Account Number cannot be null.");
		}
		
		//PIN -	should not dispense funds if the pin is incorrect,
		
		if(request.getPin() == null || request.getPin().equals("")) {
			throw new BusinessException("PIN cannot be null.");
		}
		
		//Validating username and password in BD
		Integer validation = clientAccountDao.validateAccountNumberAndPin(request.getAccountNumber(), request.getPin());
		if(validation == -1) {
			throw new BusinessException("AccountNumber does not exists.");
		}else if(validation == -2) {
			throw new BusinessException("PIN Incorrect.");
		}else if (validation == 0) {
			clientAccount = clientAccountDao.findBalanceByAccountNumberAndPin(request.getAccountNumber(), request.getPin());
			clientAccount.setMaxWithDrawal(clientAccount.getOpeningBalance() + clientAccount.getOverdraft());
		}
		
		//Getting the balance of the ATM machine (number of notes and total)
		atm = atmDao.getAtmBalance();
		
		//-	cannot dispense more money than it holds,
		if (atm.total < request.getAmount()) {
			throw new BusinessException("ATM does not have enough notes for your request.");
		}
		
		//-	cannot dispense more funds than customer have access to
		if(request.getAmount() > (clientAccount.getOpeningBalance() + clientAccount.overdraft)) {
			throw new BusinessException("Client does not have enough funds.");
		}
		
		//logic withdrawal
		int notes[] =  {50, 20, 10, 5};
		int[] noteCounter = new int[notes.length];
		int[] noteCounterAtm =  {atm.getFiftys(), atm.getTwentys(), atm.getTens(), atm.getFives()};
		
		int amount = request.getAmount();
		
		//should dispense the minimum number of notes per withdrawal,
		for(int i=0; i < noteCounter.length; i++) {
			if(amount>=notes[i]) {
				noteCounter[i] = amount/notes[i]; //integer division
				
				//atm does not have enough notes
				if(noteCounter[i] > noteCounterAtm[i]) {
					noteCounter[i] = noteCounterAtm[i];
				}
				
				amount = amount - noteCounter[i] * notes[i]; //diff with amount
			}
		}
		
		
		//counting quantity of notes
		String quantityNotes = "";
		for(int i=0; i < noteCounter.length; i++) {
			if(noteCounter[i]>0) {
				switch(i) {
					case 0:
						quantityNotes = quantityNotes + noteCounter[i] + " notes of 50 euros. ";
						break;
					case 1:
						quantityNotes = quantityNotes + noteCounter[i] + " notes of 20 euros. ";
						break;
					case 2:
						quantityNotes = quantityNotes + noteCounter[i] + " notes of 10 euros. ";
						break;
					case 3:
						quantityNotes = quantityNotes + noteCounter[i] + " notes of 5 euros. ";
						break;
				}
					
			}
		}
		
		//if amount == 0 means that the ATM has enough notes for the transaction so it's needed to update the
		//balance of the ATM.
		if (amount == 0) {
			atm = atmDao.updateBalance(noteCounter);
		}
		//-	should only dispense the exact amounts requested,
		else if (amount != 0) {
			throw new BusinessException("ATM does not have enough notes for the transaction.");
		}
		
		//everything is OK so it's necessary update the client balance
		clientAccount = clientAccountDao.updateBalance(request.getAccountNumber(), request.getPin(),request.getAmount());
		
		withDrawResponse.setAccountNumber(clientAccount.getAccountNumber()); 
		withDrawResponse.setOpeningBalance(clientAccount.getOpeningBalance()); 
		withDrawResponse.setOverdraft(clientAccount.getOverdraft()); 
		withDrawResponse.setQuantityNotes(quantityNotes); 
		withDrawResponse.setMaxWithDrawal(clientAccount.getOpeningBalance() + clientAccount.getOverdraft());
		withDrawResponse.setAtm(atm);
		return withDrawResponse;
	}
	
}
