FROM openjdk:11
EXPOSE 8080
ADD target/Assessment_ZinkWorks-0.0.1-SNAPSHOT.jar Assessment_ZinkWorks-0.0.1-SNAPSHOT.jar 
ENTRYPOINT ["java","-jar","/Assessment_ZinkWorks-0.0.1-SNAPSHOT.jar"]