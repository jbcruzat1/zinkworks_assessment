CREATE TABLE public.account
(
    accountNumber integer NOT NULL,
    pin integer,
    openingBalance NUMERIC,
    overdraft NUMERIC,
    CONSTRAINT pk_account PRIMARY KEY (accountNumber)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;





CREATE OR REPLACE FUNCTION public.validateAccountNumberAndPin(
	in_accountNumber integer,
	in_pin integer)
    RETURNS INTEGER
	LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
	r account;
BEGIN
	IF not exists(select 1 from account where accountNumber = in_accountNumber) THEN
		return -1;
	END IF;
	    	
	IF not exists(select 1 from account where accountNumber = in_accountNumber and pin = in_pin) THEN
		return -2;
	END IF;
	
	return 0;
	
END
$BODY$;


	

CREATE OR REPLACE FUNCTION public.findBalanceByAccountNumberAndPin(
	in_accountNumber integer,
	in_pin integer)
    RETURNS SETOF public.account 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
	r account;
	query TEXT;
BEGIN
	query:='SELECT *
	    	FROM account
	    	WHERE accountNumber = ' || in_accountNumber || ' AND pin = ' || in_pin;
	    	
	for r IN execute query
	LOOP
		return next r;
	END LOOP;
    	RETURN;
END
$BODY$;



CREATE OR REPLACE FUNCTION public.updateClientAccount(
	in_accountNumber integer,
	in_pin integer,
	in_amount integer)
    RETURNS SETOF public.account 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
	r account;
	oldOpeningBalance integer;
	oldOverdraft integer;
	newOpeningBalance integer;
	newOverdraft integer;
	newAmount integer;
BEGIN
	
	newAmount = in_amount;
	
	SELECT openingBalance , overdraft into oldOpeningBalance, oldOverdraft from account WHERE accountNumber = in_accountNumber AND pin = in_pin; 
	
	--It means that the opening balance is not enough for all the amount the client withdrawed
	IF(oldOpeningBalance < newAmount) THEN
		newOpeningBalance = 0;
		newAmount = newAmount - oldOpeningBalance;
		newOverdraft = oldOverdraft - newAmount;
	--It means that the opening balance is enough for all the amount the client withdrawed, so the overdraft remains with the same value
	ELSIF (oldOpeningBalance >= newAmount)  THEN
		newOpeningBalance = oldOpeningBalance - newAmount;
		newOverdraft = oldOverdraft;
	END IF;
	

	UPDATE account
	SET openingBalance = newOpeningBalance , overDraft = newOverdraft
	WHERE accountNumber = in_accountNumber AND pin = in_pin
	
	RETURNING * INTO r;
	
    	IF NOT(r IS NULL) THEN 
		RETURN NEXT r;
	END IF;
	
	RETURN;

END
$BODY$;


CREATE TABLE public.atm
(
	total numeric,
    fiftys integer ,
    twentys integer,
    tens integer,
    fives integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE OR REPLACE FUNCTION public.findNotesAtm()
    RETURNS SETOF public.atm 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
	query TEXT;
BEGIN
	RETURN QUERY
	SELECT TOTAL,FIFTYS,TWENTYS,TENS,FIVES FROM atm;

END
$BODY$;


CREATE OR REPLACE FUNCTION public.updateBalance(
	in_fiftys integer,
	in_twentys integer,
	in_tens integer,
	in_fives integer)
    RETURNS SETOF public.atm 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
	r atm;
	query TEXT;
BEGIN

	UPDATE atm
	SET total = (50 * (fiftys - in_fiftys)) + (20 * (twentys - in_twentys)) + (10 * (tens - in_tens)) + (5 * (fives - in_fives)),
	fiftys = fiftys - in_fiftys,
	twentys = twentys - in_twentys,
	tens = tens - in_tens,
	fives = fives - in_fives
	RETURNING * INTO r;
	
    	IF NOT(r IS NULL) THEN 
		RETURN NEXT r;
	END IF;
	
	RETURN;

END
$BODY$;


delete from public.account;
delete from public.atm;
insert into public.account values (123456789,1234,800,200);
insert into public.account values (987654321,4321,1230,150);
insert into public.atm values(1500,10,30,30,20);
